﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Task2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        static int score = 0;
        static int random = 0;
        static int guesses = 0;

        public void buttonClick(object sender, EventArgs e)
        {
            Background.BackgroundColor = Color.FromHex($"#FFF");

            int guess = 0;
            guess = int.Parse(numberEntry.Text);
            
            var x = new function();

            var ran = x.randomNum(random);

            if (x.randomNum(random) == guess)
            {
                score = score + 1;
                Background.BackgroundColor = Color.FromHex($"#47d147");
            }
            else
            {
                Background.BackgroundColor = Color.FromHex($"#ff3333");
            }

            guesses++;
            label2.Text = ("Number of games won: " + score);
            label4.Text = ("I guessed: " + ran);
            label5.Text = ("Number of guesses: " + guesses);
        }

    }
}
