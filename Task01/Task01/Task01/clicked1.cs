﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01
{
    class clicked1
    {
        public static int click1(string number)
        {
            int num, i, result = 0;

            num = int.Parse(number);

            for (i = 0; i < num; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {

                    result = result + i;
                }
            }
            return result;
        }
    }
}
