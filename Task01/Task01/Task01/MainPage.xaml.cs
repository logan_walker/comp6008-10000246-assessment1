﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Task01
{
    public partial class MainPage : ContentPage
    {

        static string answer = "Your answer: ";

        public MainPage()
        {
            InitializeComponent();
        }

        void buttonClick(object sender, EventArgs e)
        {
            var input = clicked1.click1(numberEntry.Text);
            label3.Text = answer + input;
        }
    }
}
